from AlgoritmosGeneticos import *

pesoMax = int(input("Qual o peso que o caminhão suporta? "))  # (20) Peso maximo que o caminhao suporta
volumeMax = int(input("Qual o volume que o baú do caminhão comporta? "))  # (30) Peso maximo que cabe no caminhao

# produto = [peso, volume]
produtos = []
#produtos = [[10, 5], [8, 3], [4, 11], [1, 5], [8, 11], [3, 8], [7, 15], [7, 11], [3, 12], [5, 10]]

print()
print("Hora de informar os produtos. Informe 0 (zero) no peso ou volume para parar.")
while (True):
    produtoAtual = len(produtos) + 1

    peso = int(input(f"Produto {produtoAtual} | Informe o peso: "))
    if (peso == 0):
        print("Insersão de novos produtos encerrada")
        break

    volume = int(input(f"Produto {produtoAtual} | Informe o volume: "))
    if (volume == 0):
        print("Insersão de novos produtos encerrada")
        break

    produto = [peso, volume]
    produtos.append(produto)
    print()

print()
geracoes = int(input("Qual a quantidade de gerações que serão criadas? ")) - 1  # 50
nroIndividuosGeracao = int(input("Quantos indivíduos farão parte de cada geração? "))  # 50
taxaMutacao = int(input("Qual a taxa de possibilidade de mutação (em %) de um indivíduo? ")) / 100  # 5
print()

qtdeProdutos = len(produtos)

# Inicia os trabalhos de evolucao
populacao = gerarPopulacao(nroIndividuosGeracao, qtdeProdutos)
historicoAvaliacoes = []

avaliacaoMedia = buscarAvaliacaoMediaPopulacao(populacao, pesoMax, volumeMax, produtos)
historicoAvaliacoes.append(avaliacaoMedia)

for i in range(geracoes):
    populacao = evoluirPopulacao(populacao, pesoMax, volumeMax, produtos, nroIndividuosGeracao, taxaMutacao)
    avaliacaoMedia = buscarAvaliacaoMediaPopulacao(populacao, pesoMax, volumeMax, produtos)
    historicoAvaliacoes.append(avaliacaoMedia)

# Prints para visualização
for indice, mediaProdutosGeracao in enumerate(historicoAvaliacoes):
    print("Geração: ", indice, " | Média de produtos no caminhão: ", mediaProdutosGeracao)

#print("Peso máximo: ", pesoMax, "\nVolume máximo: ", volumeMax, "\n\nProdutos:")
print("\nProdutos:")
for indice, produto in enumerate(produtos):
    print("Item ", indice + 1, " -> Peso: ", produto[0], " | Volume: ", produto[1])

#print("\n 10 exemplos de boas soluções: ")
#for i in range(10):
#    print(populacao[i])

melhoresSolucoes = []
melhorAvaliacao = 0
for i in range(len(populacao)):
    avaliacao = avaliarIndividuo(populacao[i], pesoMax, volumeMax, produtos)
    if (avaliacao < 0):
        continue

    if (avaliacao > melhorAvaliacao):
        melhorAvaliacao = avaliacao
        melhoresSolucoes = []
        melhoresSolucoes.append(populacao[i])
    elif (avaliacao == melhorAvaliacao and populacao[i] not in melhoresSolucoes):
        melhoresSolucoes.append(populacao[i])

print("\nMelhores soluções")
for i in range(len(melhoresSolucoes)):
    print(melhoresSolucoes[i])

# Monta o gráfico
from matplotlib import pyplot as plt
plt.plot(range(len(historicoAvaliacoes)), historicoAvaliacoes)
plt.grid(True, zorder = 0)
plt.title("Problema da mochila aplicado a carga de um caminhão")
plt.xlabel("Geração")
plt.ylabel("Qtde média de itens no caminhão")
plt.show()
