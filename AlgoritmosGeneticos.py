from random import getrandbits, random, randint

def gerarIndividuo(qtdeProdutos):  # Gera um indivíduo para a população
    individuo = []
    for x in range(qtdeProdutos):
        individuo.append(getrandbits(1))

    return individuo

def gerarPopulacao(nroIndividuosGeracao, qtdeProdutos):  # Gera uma população
    populacao = []
    for x in range(nroIndividuosGeracao):
        populacao.append(gerarIndividuo(qtdeProdutos))

    return populacao

def avaliarIndividuo(individuo, pesoMaximo, volumeMaximo, produtos):  # Faz a avaliação (fitness) de um indivíduo
    pesoTotal = 0
    volumeTotal = 0

    for indice, temProduto in enumerate(individuo):
        if (temProduto == 1):
            # O produto que está na posição 'index' do array de produtos faz parte desta combinação.
            # Vamos somar seu peso e volume nos totais.
            pesoTotal += produtos[indice][0]
            volumeTotal += produtos[indice][1]

    if (pesoTotal > pesoMaximo or volumeTotal > volumeMaximo):
        return -1  # O peso e/ou volume ficaram maior do que o caminhão suporta, vamos retornar -1 para identificar o erro

    return sum(individuo)  # Vamos retornar a soma da quantidade de itens que couberam no caminhão

def buscarAvaliacaoMediaPopulacao(populacao, pesoMaximo, volumeMaximo, produtos):
    # Busca a avaliação média da população, levando em conta apenas os indivíduos que couberem no caminhão (que respeitem seu peso e volume máximos)
    somaAvaliacao = 0
    tamanhoPopulacao = len(populacao)

    for individuo in populacao:
        avaliacao = avaliarIndividuo(individuo, pesoMaximo, volumeMaximo, produtos)
        if (avaliacao >= 0):
            somaAvaliacao += avaliacao

    return somaAvaliacao / tamanhoPopulacao

def selecionarPais(pais):
    # Faz uma seleção, através da regra da roleta, de um pai e mãe para fazer a evolução da população
    def sortearPai(avaliacaoTotal, ignorarIndividuo=-1):
        roleta = []
        acumulado = 0
        nroSorteado = random()

        if (ignorarIndividuo > 0):
            avaliacaoTotal -= valores[0][ignorarIndividuo]

        for indice, i in enumerate(valores[0]):
            if (ignorarIndividuo == indice):
                continue

            acumulado += i
            roleta.append(acumulado / avaliacaoTotal)
            if (roleta[-1] >= nroSorteado):
                return indice

        return -1

    # print(pais)
    # print(*pais)
    # print(zip(*pais))
    # print(list(zip(*pais)))

    valores = list(zip(*pais))
    avaliacaoTotal = sum(valores[0])

    indexPai = sortearPai(avaliacaoTotal)
    indexMae = sortearPai(avaliacaoTotal, indexPai)
    # print("Index pai: ", indexPai)
    # print("Index mae: ", indexMae)
    # print("\n\n")

    while (indexPai == -1 or indexMae == -1):
        valores = list(zip(*pais))
        avaliacaoTotal = sum(valores[0])

        indexPai = sortearPai(avaliacaoTotal)
        indexMae = sortearPai(avaliacaoTotal, indexPai)

        # print("Index pai: ", indexPai)
        # print("Index mae: ", indexMae)
        # print("\n\n")

    # print("Índice do pai: ", indexPai)
    # print("Índice da mãe: ", indexMae)
    # print("\n")

    pai = valores[1][indexPai]
    mae = valores[1][indexMae]

    return pai, mae

def evoluirPopulacao(populacao, pesoMaximo, volumeMaximo, produtos, nroIndividuosGeracao, taxaMutacao = 0.05):
    pais = []
    for individuo in populacao:
        avaliacao = avaliarIndividuo(individuo, pesoMaximo, volumeMaximo, produtos)
        if (avaliacao >= 0):
            pais.append([avaliacao, individuo])

    if (len(pais) == 0):  # Não conseguiu encontrar pais aptos, vamos retornar a propria populacao
        return populacao

    pais.sort(reverse=True)  # Coloca os individuos com mais produtos por primeiro

    # Inicia a reprodução
    filhos = []
    while (len(filhos) < nroIndividuosGeracao):
        pai, mae = selecionarPais(pais)
        metade = len(pai) // 2  # As duas barras (//) retornar o inteiro mais próximo para a divisão feita
        filho = pai[:metade] + mae[metade:]  # Gera um filho com os genes (produtos) iniciais do pai e os genes finais da mãe
        filhos.append(filho)

    # Agora vamos adicionar uma mutação em alguns filhos
    for individuo in filhos:
        if (taxaMutacao <= random()):
            continue

        posicaoMutacao = randint(0, len(individuo) - 1)
        if (individuo[posicaoMutacao] == 1):
            individuo[posicaoMutacao] = 0
        else:
            individuo[posicaoMutacao] = 1

    return filhos
